import java.util.List;

public class ASTTimes  implements ASTNode {

	public ASTNode left;
	public ASTNode right;
	
	public ASTTimes(ASTNode newLeft, ASTNode newRight) {
		left = newLeft;
		right = newRight;
	}
	/*
	@Override
	public int eval(Environment e) {
		int v1 = left.eval(e);
		int v2 = right.eval(e);
		return v1*v2;
	}*/
	
	
	 @Override
	 public IValue eval(Enviroment<IValue> e){
	 	IValue v1 = left.eval(e);
	 	if(v1 instanceof VInt){
	 		IValue v2 = right.eval(e);
	 		if(v2 instanceof VInt){
	 			return new VInt(((VInt) v1).getValue() * ((VInt) v2).getValue());
	 		}
	 	}
	 	throw new Error("illegal arguments to * operator");
	 }
	 

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		left.compile(e, code);
		right.compile(e, code);
		//code.add("	sipush " + left.eval(e));
		//code.add("	sipush " + right.eval(e));
		code.add("	imul ");
	}


	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TInt.getInstance();
	}

}
