
public class TRef implements IType {

	private String nameType;
	private static TRef single_instance = null;
	
	public TRef() {
		nameType = "ref";
	}
	
	public static TRef getInstance()
    {
        if (single_instance == null)
            single_instance = new TRef();
 
        return single_instance;
    }

	@Override
	public void show() {
		System.out.println(nameType);
	}

}
