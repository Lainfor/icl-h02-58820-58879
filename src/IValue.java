
public interface IValue {
	void show();
	String valueToString();
}
