import java.util.List;

public class ASTAnd implements ASTNode {
	
	private ASTNode left;
	private ASTNode right;
	
	public ASTAnd(ASTNode newLeft, ASTNode newRight) {
		left = newLeft;
		right = newRight;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		IValue v1 = left.eval(e);
		IValue v2 = right.eval(e);
		
		if(v1 instanceof VBool && v2 instanceof VBool) {
			return new VBool(((VBool) v1).getValue() && ((VBool) v2).getValue());
		}
		
		return null;
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		left.compile(e, code);
		right.compile(e, code);
		code.add("	iand");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TBool.getInstance();
	}

}
