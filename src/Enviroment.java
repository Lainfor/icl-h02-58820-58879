import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Enviroment<E> {
	
	//public List<Integer> vars;
	public HashMap<String, E> vars;
	public Enviroment<E> envP; //parent enviorement
	
	public Enviroment() {
		envP = null;
		vars = new HashMap<>();
	}
	
	public Enviroment(Enviroment<E> p) {
		envP = p;
		vars = new HashMap<>();
	}

	public Enviroment<E> beginScope() {
		// Criar ambiente filho
		Enviroment<E> e = new Enviroment<E>(this);
		return e;
	}

	public void add(String id, E value) {
		// Adicionar let ao ambiente atual
		vars.put(id, value);
	}

	public E find(String id) {
		// Devolver um let do ambiente
		E aux = vars.get(id);
		if(aux == null) {
			aux = envP.find(id);
		}
		return aux;
	}

	public Enviroment<E> endScope() {
		// Libertar no atual
		return envP;
	}

	/*@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return (IType) E;
	}*/
	
}
