import java.util.List;

public class ASTNum implements ASTNode {

	private int val;
	
	public ASTNum(int newVal) {
		val = newVal;
	}
	
	@Override
	public IValue eval(Enviroment<IValue> e) {
		return new VInt(val);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("	aload 4");
		code.add("	sipush " + val);
		
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TInt.getInstance();
	}

}
