
public class VInt implements IValue{
	
	private int value;
	
	public VInt(int n) {
		value = n;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public String valueToString() {
		return "" + value;
	}

	@Override
	public void show() {
		System.out.println(value);
	}
}
