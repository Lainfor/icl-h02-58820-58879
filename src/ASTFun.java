import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTFun implements ASTNode {
	
	private String fName;
	private List<ASTNode> fArgs;
	private List<IType> types;
	private List<ASTNode> values;
	private List<ASTNode> fBody;
	
	public ASTFun(String newFName, List<ASTNode> newFArgs, List<IType> newTypes, List<ASTNode> newFBody) {
		fName = newFName;
		fArgs = newFArgs;
		types = newTypes;
		fBody = newFBody;
		values = null;
	}
	
	public void setValues(List<ASTNode> newVals) {
		values = newVals;
	}
	
	public String getFunName() {
		return fName;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		e.beginScope();
		for(int i=0; i<fArgs.size(); i++) {
			if(i >= values.size()) {
				for(int j=i; j<fArgs.size(); j++) {
					ASTNode arg = fArgs.get(j);
					String id = ((ASTId) arg).getCode();
					e.add(id, null);
				}
			}else {
				ASTNode arg = fArgs.get(i);
				ASTNode val = values.get(i);
				String id = ((ASTId) arg).getCode();
				IValue v = val.eval(e);
				e.add(id, v);
			}
		}
		IValue v = null;
		for(ASTNode fb: fBody) {
			v = fb.eval(e);
		}
		//IValue v = fBody.eval(e);
		e.endScope();
		return v;
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("	new f" + fName);
		code.add("	dup");
		code.add("		invokespecial f" + fName + "/<init>()V");
		code.add("	dup");
		code.add("	aload 4");
		code.add("	putfield f" + fName + "/sl Ljava/lang/Object;");
		code.add("	astore 4");
		for(ASTNode nd: fArgs) {
			nd.compile(e, code);
			//code.add("  putfiled "+ fName + "/" + ((ASTId) nd).getCode() + "0 I");
		}
		for(ASTNode fb: fBody) {
			fb.compile(e, code);
		}
		//fBody.compile(e, code);
		code.add("	aload 4");
		code.add("	getfield f" + fName + "/sl Lf" + fName);
		code.add("	astore 4");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		for(int i=0; i<fArgs.size(); i++) {
			ASTNode arg = fArgs.get(i);
			IType typ = types.get(i);
			e.add(((ASTId) arg).getCode(), typ);
		}
		e.add(fName, TFun.getInstance());
		return TFun.getInstance();
	}

}
