import java.util.List;

public class ASTIf implements ASTNode {
	
	private ASTNode cond;
	private List<ASTNode> body1;
	private List<ASTNode> body2;
	
	public ASTIf(ASTNode newCond, List<ASTNode> newBody1, List<ASTNode> newBody2) {
		cond = newCond;
		body1 = newBody1;
		body2 = newBody2;
	}
	
	@Override
	public IValue eval(Enviroment<IValue> e) {
		IValue v2 = null;
		if(((VBool)cond.eval(e)).getValue()) {
			for(ASTNode fb: body1) {
				v2 = fb.eval(e);
			}
			return v2;
		}else {
			for(ASTNode fb: body2) {
				v2 = fb.eval(e);
			}
			return v2;
		}
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		cond.compile(e, code);
		code.add("  ifeq L1");
		for(ASTNode fb: body1) {
			fb.compile(e, code);
		}
		//body1.compile(e, code);
		code.add("	goto L2");
		code.add("	L1:");
		for(ASTNode fb: body2) {
			fb.compile(e, code);
		}
		//body2.compile(e, code);
		code.add("	L2:");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return cond.typecheck(e);
	}

}
