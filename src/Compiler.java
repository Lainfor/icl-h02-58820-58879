import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class Compiler {
	
	private boolean result;
	
	public Compiler() {
		result = false;
	}
	
	public boolean compileCode(Enviroment e, ASTNode code) {
		IValue res = code.eval(e);
		if(((VInt) res).getValue() != 0) {
			setCompiler(true);
			return result;
		}
		setCompiler(false);
		return result;
	}
	
	public boolean getCompiler() {
		return result;
	}
	
	public void setCompiler(boolean newRes) {
		result = newRes;
	}
	
	public void printFile(List<String> code) {
		try {
			PrintStream writer = new PrintStream(new FileOutputStream("test.j"));
			for(String line: code) {
				writer.println(line);
			}
			writer.close();
		}catch(FileNotFoundException e) {
			System.out.println("Syntax Error!");
		}
	}
	
	public static void clearTheFile(){
        FileWriter fwOb = null;
		try {
			fwOb = new FileWriter("test.j", false);
		} catch (IOException e) {
			e.printStackTrace();
		} 
        PrintWriter pwOb = new PrintWriter(fwOb, false);
        pwOb.flush();
        pwOb.close();
        try {
			fwOb.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
