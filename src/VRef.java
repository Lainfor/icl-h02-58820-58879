
public class VRef implements IValue {
	
	public IValue value;
	
	public VRef(IValue v) {
		value = v;
	}
	
	public IValue getValue() {
		return value;
	}
	
	public void setValue(IValue v) {
		value = v;
	}

	@Override
	public void show() {
		value.show();
	}

	@Override
	public String valueToString() {
		return "" + value;
	}

}
