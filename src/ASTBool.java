import java.util.List;

public class ASTBool implements ASTNode {
	
	private boolean value;
	
	public ASTBool(boolean newVal) {
		value = newVal;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		return new VBool(value);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		if(value) {
			code.add("	sipush 1");
		}else {
			code.add("	sipush 0");
		}

	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TBool.getInstance();
	}

}
