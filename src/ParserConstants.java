/* Generated By:JavaCC: Do not edit this line. ParserConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int TYPEDEF = 4;
  /** RegularExpression Id. */
  int ENDLINE = 5;
  /** RegularExpression Id. */
  int NE = 6;
  /** RegularExpression Id. */
  int EQ = 7;
  /** RegularExpression Id. */
  int GE = 8;
  /** RegularExpression Id. */
  int LE = 9;
  /** RegularExpression Id. */
  int GT = 10;
  /** RegularExpression Id. */
  int LT = 11;
  /** RegularExpression Id. */
  int OR = 12;
  /** RegularExpression Id. */
  int AND = 13;
  /** RegularExpression Id. */
  int LET = 14;
  /** RegularExpression Id. */
  int IN = 15;
  /** RegularExpression Id. */
  int END = 16;
  /** RegularExpression Id. */
  int INT = 17;
  /** RegularExpression Id. */
  int BOOL = 18;
  /** RegularExpression Id. */
  int REF = 19;
  /** RegularExpression Id. */
  int FUN = 20;
  /** RegularExpression Id. */
  int CALL = 21;
  /** RegularExpression Id. */
  int IF = 22;
  /** RegularExpression Id. */
  int THEN = 23;
  /** RegularExpression Id. */
  int ELSE = 24;
  /** RegularExpression Id. */
  int WHILE = 25;
  /** RegularExpression Id. */
  int DO = 26;
  /** RegularExpression Id. */
  int TRUE = 27;
  /** RegularExpression Id. */
  int FALSE = 28;
  /** RegularExpression Id. */
  int Id = 29;
  /** RegularExpression Id. */
  int Num = 30;
  /** RegularExpression Id. */
  int IdVars = 31;
  /** RegularExpression Id. */
  int EQUAL = 32;
  /** RegularExpression Id. */
  int PLUS = 33;
  /** RegularExpression Id. */
  int MINUS = 34;
  /** RegularExpression Id. */
  int TIMES = 35;
  /** RegularExpression Id. */
  int DIV = 36;
  /** RegularExpression Id. */
  int LPAR = 37;
  /** RegularExpression Id. */
  int RPAR = 38;
  /** RegularExpression Id. */
  int EL = 39;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\r\"",
    "\":\"",
    "\";\"",
    "\"!=\"",
    "\"==\"",
    "\">=\"",
    "\"<=\"",
    "\">\"",
    "\"<\"",
    "\"||\"",
    "\"&&\"",
    "\"let\"",
    "\"in\"",
    "\"end\"",
    "\"int\"",
    "\"bool\"",
    "\"ref\"",
    "\"fun\"",
    "\"call\"",
    "\"if\"",
    "\"then\"",
    "\"else\"",
    "\"while\"",
    "\"do\"",
    "\"true\"",
    "\"false\"",
    "<Id>",
    "<Num>",
    "<IdVars>",
    "\"=\"",
    "\"+\"",
    "\"-\"",
    "\"*\"",
    "\"/\"",
    "\"(\"",
    "\")\"",
    "\"\\n\"",
  };

}
