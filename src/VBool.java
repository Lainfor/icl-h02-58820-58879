
public class VBool implements IValue{
	
	private boolean value;
	
	public VBool(boolean t) {
		value = t;
	}
	
	public boolean getValue() {
		return value;
	}

	@Override
	public void show() {
		System.out.println(value);
	}

	@Override
	public String valueToString() {
		// TODO Auto-generated method stub
		return null;
	}
}
