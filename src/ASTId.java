import java.util.List;

public class ASTId implements ASTNode{

	private String id;
	
	public ASTId(String id) {
		this.id = id;
	}
	
	
	public String getCode() {
		return id;
	}
	
	@Override
	public IValue eval(Enviroment<IValue> e) {
		return (IValue) e.find(id);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("  aload 4");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return (IType) e.find(id);
	}
	
	
}
