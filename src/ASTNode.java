import java.util.List;

public interface ASTNode {
	IValue eval(Enviroment<IValue> e);
	//int eval(Environment e);
	void compile(Enviroment<String> e, List<String> code);
	IType typecheck(Enviroment<IType> e);
}
