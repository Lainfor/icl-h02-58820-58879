import java.util.List;

public class ASTDiv implements ASTNode {

	public ASTNode left;
	public ASTNode right;
	
	public ASTDiv(ASTNode newLeft, ASTNode newRight) {
		left = newLeft;
		right = newRight;
	}
	
	 @Override
	 public IValue eval(Enviroment<IValue> e){
	 	IValue v1 = left.eval(e);
	 	if(v1 instanceof VInt){
	 		IValue v2 = right.eval(e);
	 		if(v2 instanceof VInt){
	 			return new VInt(((VInt) v1).getValue() / ((VInt) v2).getValue());
	 		}
	 	}
	 	throw new Error("illegal arguments to / operator");
	 }
	 

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		left.compile(e, code);
		right.compile(e, code);
		code.add("	idiv ");
	}


	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TInt.getInstance();
	}
}
