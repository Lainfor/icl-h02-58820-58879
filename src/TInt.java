
public class TInt implements IType {
	
	private String nameType;
	private static TInt single_instance = null;
	
	public TInt() {
		nameType = "int";
	}
	
	public static TInt getInstance()
    {
        if (single_instance == null)
            single_instance = new TInt();
 
        return single_instance;
    }

	@Override
	public void show() {
		System.out.println(nameType);
	}

}
