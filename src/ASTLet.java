import java.util.List;

public class ASTLet implements ASTNode {
	public String id;
	public ASTNode number;
	public List<ASTNode> body;
	public IType type;
	
		
	public ASTLet(String id, ASTNode number, List<ASTNode> body, IType type) {
		this.id = id;
		this.number = number;
		this.body = body;
		this.type=type;
	}
	

	@Override
	public IValue eval(Enviroment<IValue> e) {
		IValue v1 = number.eval(e);
		
		e.beginScope();
		e.add(this.id, v1);
		
		IValue v2 = null;
		for(ASTNode fb: body) {
			v2 = fb.eval(e);
		}
		//IValue v2 = body.eval(e);
		
		e.endScope();
		return v2;
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("	new f" + id);
		code.add("	dup");
		code.add("		invokespecial f" + id + "/<init>()V");
		code.add("	dup");
		code.add("	aload 4");
		code.add("	putfield f" + id + "/sl Ljava/lang/Object;");
		code.add("	astore 4");
		number.compile(e, code);
		for(ASTNode fb: body) {
			fb.compile(e, code);
		}
		//body.compile(e, code);
		code.add("	aload 4");
		code.add("	getfield f" + id + "/sl Lf" + id);
		code.add("	astore 4");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		IType t = number.typecheck(e);
		e.add(id, t);
		if(t.getClass().getName().equals(type.getClass().getName())) {
			IType t1 = null;
			for(ASTNode fb: body) {
				t1 = fb.typecheck(e);
			}
			return t1;
			//return body.typecheck(e);
		}
		return null;
	}
}
