import java.util.ArrayList;
import java.util.List;

public class ASTCall implements ASTNode {
	
	private String fun;
	private ArrayList<ASTNode> parameters;
	
	public ASTCall(String newFunction, ArrayList<ASTNode> newParameters) {
		fun = newFunction;
		parameters = newParameters;
	}
	
	public String getFunName() {
		return fun;
	}
	
	public ArrayList<ASTNode> getParams(){
		return parameters;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		return e.find(fun);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("  gettfiled " + fun + "0 I");
		for(ASTNode nd: parameters) {
			nd.compile(e, code);
		}
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return e.find(fun);
	}

}
