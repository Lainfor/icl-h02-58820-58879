import java.util.List;

public class ASTOr implements ASTNode {
	
	private ASTNode left;
	private ASTNode right;
	
	public ASTOr(ASTNode newLeft, ASTNode newRight) {
		left = newLeft;
		right = newRight;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		IValue v1 = left.eval(e);
		IValue v2 = right.eval(e);
		
		return new VBool(((VBool) v1).getValue() || ((VBool) v2).getValue());
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		left.compile(e, code);
		right.compile(e, code);
		code.add("	ior");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TBool.getInstance();
	}

}
