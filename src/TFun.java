
public class TFun implements IType {
	
	private String nameType;
	private static TFun single_instance = null;
	
	public TFun() {
		nameType = "function";
	}
	
	public static TFun getInstance()
    {
        if (single_instance == null)
            single_instance = new TFun();
 
        return single_instance;
    }

	@Override
	public void show() {
		System.out.println(nameType);
	}

}
