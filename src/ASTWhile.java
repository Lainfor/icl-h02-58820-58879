import java.util.List;

public class ASTWhile implements ASTNode {
	
	private ASTNode cond;
	private List<ASTNode> body;
	
	public ASTWhile(ASTNode newCond, List<ASTNode> newBody){
		cond = newCond;
		body = newBody;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		//IValue v = null;
		while(((VBool)cond.eval(e)).getValue()) {
			//v = body.eval(e);
			for(ASTNode fb: body) {
				fb.eval(e);
			}
			//body.eval(e);
		}
		
		return (VBool) cond.eval(e);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		code.add("  L1:");
		cond.compile(e, code);
		code.add("  ifeq L2");
		for(ASTNode fb: body) {
			fb.compile(e, code);
		}
		//body.compile(e, code);
		code.add("  goto L1:");
		code.add("  L2:");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		IType t = null;
		for(ASTNode fb: body) {
			fb.typecheck(e);
		}
		return t;
	}

}
