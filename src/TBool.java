
public class TBool implements IType {
	
	private String nameType;
	private static TBool single_instance = null;
	
	public TBool() {
		nameType = "bool";
	}
	
	public static TBool getInstance()
    {
        if (single_instance == null)
            single_instance = new TBool();
 
        return single_instance;
    }

	@Override
	public void show() {
		System.out.println(nameType);
	}

}
