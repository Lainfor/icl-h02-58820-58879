import java.util.List;

public class ASTAssign implements ASTNode {
	private ASTNode node;
	private String id;
	private IType type;
	
	public ASTAssign(ASTNode nd,String newId, IType type) {
		node = nd;
		id = newId;
		this.type = type;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		e.add(id, node.eval(e));
		return node.eval(e);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {		
		node.compile(e, code);
		//code.add("  putfiled " + id + "0 I");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		if(node.typecheck(e).getClass().getName().equals(type.getClass().getName())) {
			e.add(id, node.typecheck(e));
			return node.typecheck(e);
		}
		return null;
	}

}
