import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ASTComp implements ASTNode {
	
	private String symbol;
	private ASTNode left;
	private ASTNode right;
	
	public ASTComp(String newSymbol, ASTNode newLeft, ASTNode newRight) {
		if(newSymbol == "<" || newSymbol == ">" || newSymbol == ">=" || newSymbol == "<=" || newSymbol == "==" || newSymbol == "!=") {
			symbol = newSymbol;
		}else {
			symbol = "";
		}
		
		left = newLeft;
		right = newRight;
	}

	@Override
	public IValue eval(Enviroment<IValue> e) {
		//ScriptEngineManager manager = new ScriptEngineManager();
		//ScriptEngine engine = manager.getEngineByName("js");
		
		IValue v1 = left.eval(e);
		IValue v2 = right.eval(e);
		
		//String aux = "" + v1 + symbol + v2;
		
		boolean result = false;
		switch(symbol) {
			case "<":
				result = ((VInt)v1).getValue() < ((VInt)v2).getValue();
				break;
			case ">":
				result = ((VInt)v1).getValue() > ((VInt)v2).getValue();
				break;
			case "<=":
				result = ((VInt)v1).getValue() <= ((VInt)v2).getValue();
				break;
			case ">=":
				result = ((VInt)v1).getValue() >= ((VInt)v2).getValue();
				break;
			case "==":
				result = ((VInt)v1).getValue() == ((VInt)v2).getValue();
				break;
			case "!=":
				result = ((VInt)v1).getValue() != ((VInt)v2).getValue();
				break;
		}
		/*try {
			result = (boolean) engine.eval(aux);
		} catch (ScriptException e1) {
			e1.printStackTrace();
		}*/
		
		return new VBool(result);
	}

	@Override
	public void compile(Enviroment<String> e, List<String> code) {
		left.compile(e, code);
		right.compile(e, code);
		code.add("  ifgt L1");
		code.add("  sipush 0");
		code.add("  goto L2");
		code.add("  ");
		code.add("  L1:");
		code.add("  sipush 1");
		code.add("  L2:");
	}

	@Override
	public IType typecheck(Enviroment<IType> e) {
		return TBool.getInstance();
	}

}
